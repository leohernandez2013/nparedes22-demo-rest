package com.demo.falabella.rest.demo_rest.service;

import com.demo.falabella.rest.demo_rest.models.Cliente;

public interface ClienteService {
	
	public Cliente generarCliente();
	public Boolean encolarCliente(Cliente cliente);
	public Boolean insertarCliente(Cliente cliente);
}
