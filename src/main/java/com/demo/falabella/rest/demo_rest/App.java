package com.demo.falabella.rest.demo_rest;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.demo.falabella.rest.demo_rest.models.Cliente;
import com.demo.falabella.rest.demo_rest.service.ClienteService;
import com.demo.falabella.rest.demo_rest.service.impl.ClienteServiceImpl;

/**
 * Hello world!
 *
 */
public class App {
	private static final Logger logger = LogManager.getLogger(App.class);
	public static void main(String[] args) throws InterruptedException{
		while(true) {
			int oksEncolamiento = 0;
			int oksInsercion = 0;
			logger.info("Se inicia consulta al WS REST para crear un nuevo cliente");
			ClienteService clienteService = new ClienteServiceImpl();
			Cliente cliente = clienteService.generarCliente();
			logger.info("Se ha creado generado correctamente el cliente desde el WS");
			imprimirCliente(cliente);

			for (int i = 0; i < args.length; i++) {
				if (args[i].equalsIgnoreCase("enviar-a-cola")) {
					logger.info("Inicia proceso de enviar cliente a cola MQ.");
					boolean resultadoEjecucion = clienteService.encolarCliente(cliente);
					if (resultadoEjecucion) {
						logger.info("Se ha enviado correctamente el cliente a la cola");
					} else {
						logger.error("No se ha enviado el cliente a la cola");
					}
					oksEncolamiento = oksEncolamiento + 1;
				}

				if (args[i].equalsIgnoreCase(("insertar-en-db2"))) {
					boolean resultadoEjecucion = clienteService.insertarCliente(cliente);
					if (resultadoEjecucion) {
						logger.info("Se ha ingresado correctamente el cliente en la base de datos db2");
					} else {
						logger.error("No se ha ingresado el cliente en la base de datos db2");
					}
					oksInsercion = oksInsercion + 1;
				}
			} 

			if (oksInsercion == 0 && oksEncolamiento == 0) {
				logger.info("No se ingreso el cliente en la base de datos y tampoco fue envíado a una cola");
				logger.info(
						"en caso de querer enviar clientes a una o ambas de estas herramientas agregar los siguientes argumentos:");
				logger.info("- enviar-a-cola");
				logger.info("- insertar-en-db2");
			}
			logger.info("¿seguir creando clientes? (si o no)");
			Thread.sleep(5000);
		} 
	}

	private static void imprimirCliente(Cliente cliente) {
		logger.info("Cliente:");
		String genero= "Genero: " + cliente.getGenero();
		String nombre="Nombre: " + cliente.getNombre();
		String apellido="Apellido: " +cliente.getApellido();
		String ciudad="Ciudad: " + cliente.getCiudad();
		String pais="Pais: " + cliente.getPais();
		String fecha_nacimiento="Fecha de nacimiento: " + cliente.getFechaNacimiento();
		String celular="Celular: " + cliente.getCelular();
		logger.info(genero);
		logger.info(nombre);
		logger.info(apellido);
		logger.info(ciudad);
		logger.info(pais);
		logger.info(fecha_nacimiento);
		logger.info(celular);
	}
}
