package com.demo.falabella.rest.demo_rest.dao.impl;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.demo.falabella.rest.demo_rest.dao.ClienteDao;
import com.demo.falabella.rest.demo_rest.models.Cliente;
import com.google.common.io.Resources;

public class ClienteDaoImpl implements ClienteDao {
	private static final Logger logger = LogManager.getLogger(ClienteDaoImpl.class);

	public boolean insertarCliente(Cliente cliente) {
		Boolean resultado = false;
		@SuppressWarnings("squid:S2068")
		String connectionString = "jdbc:db2://localhost:50001/testdb:" + "user=db2inst1;password=ACL.2019;";
		URL urlQuery = Resources.getResource("insert.sql");
		Connection conn = null;
		PreparedStatement prst = null;

		try {
			String query = Resources.toString(urlQuery, StandardCharsets.UTF_8);
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			conn = DriverManager.getConnection(connectionString);
			prst = conn.prepareStatement(query);
			prst.setString(1, cliente.getGenero());
			prst.setString(2, cliente.getNombre());
			prst.setString(3, cliente.getApellido());
			prst.setString(4, cliente.getCiudad());
			prst.setString(5, cliente.getPais());
			prst.setDate(6, Date.valueOf(cliente.getFechaNacimiento()));
			prst.setString(7, cliente.getCelular());
			int rs = prst.executeUpdate();
			if (rs != 0) {
				resultado = true;
			}
		} catch (SQLException | IOException | ClassNotFoundException e) {
			logger.error(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			} finally {
				try {
					if (prst != null) {
						prst.close();
					}
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
		}

		return resultado;
	}

}
