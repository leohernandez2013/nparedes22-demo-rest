package com.demo.falabella.rest.demo_rest.utils;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;



public class QueueUtils {
	
	public QueueUtils() {
		
	}
	
	private static final Logger logger = LogManager.getLogger(QueueUtils.class);
	public static Boolean encolarMensaje(String mensaje){
		Connection connection = null;
		Session session = null;
		Destination destination = null;
		MessageProducer producer = null;

		try {

			JmsFactoryFactory ff = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
			JmsConnectionFactory cf = ff.createConnectionFactory();

			cf.setStringProperty(WMQConstants.WMQ_HOST_NAME, "10.10.0.6");
			cf.setIntProperty(WMQConstants.WMQ_PORT, 1412);
			cf.setStringProperty(WMQConstants.WMQ_CHANNEL, "DEMO");
			cf.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
			cf.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, "QM_TEST");

			connection = cf.createConnection("admin","acl2019");
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue("queue:///QUEUE_TEST");
			producer = session.createProducer(destination);

			TextMessage message = session
					.createTextMessage(mensaje);
			connection.start();
			producer.send(message);
		} catch (JMSException jmsex) {
			logger.error(jmsex.getMessage());
			return false;
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (JMSException jmsex) {
				logger.error(jmsex.getMessage());
			} finally {
				try {
					if (session != null) {
						session.close();
					}
				} catch (JMSException jmsex) {
					logger.error(jmsex.getMessage());
				}finally {
					if (producer != null) {
						try {
							producer.close();
						} catch (JMSException jmsex) {
							logger.error(jmsex.getMessage());
						}
					}
				}
			}
		}
		return true;
	}
}
