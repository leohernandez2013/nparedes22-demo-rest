
# demo-rest

Demo de ejemplo de consumo de WS rest y encolamiento de mensaje en cola MQ de ibm.


## Instalación de gestor de colas mq
el comando para levantar un contenedor docker gestor de colas mq es el siguiente

```bash
docker run --env LICENSE=accept --env MQ_QMGR_NAME=QM_TEST --env MQ_ADMIN_PASSWORD=acl2019 --publish 1414:1414 --publish 9443:9443 --detach ibmcom/mq
```
Los comandos bash que se deben utilizar dentro del contenedor para configurar la cola son los siguientes:

```bash
runmqsc QM_TEST
DEFINE CHANNEL(DEMO) CHLTYPE(SVRCONN) TRPTYPE(TCP)
DEFINE LISTENER (LISTENER) TRPTYPE(TCP) CONTROL(QMGR) PORT(1412)
START LISTENER (LISTENER)
SET CHLAUTH(DEMO) TYPE(ADDRESSMAP) ADDRESS({IP_GATEWAY DEL CONTENEDOR:EX=10.0.0.1})
ALTER QMGR CHLAUTH(DISABLED) CONNAUTH(' ')
REFRESH SECURITY TYPE(CONNAUTH)
define ql('QUEQUE_TEST')

```

Dentro del código fuente del proyecto se debe modificar el archivo 
```bash
vim /demo-rest/src/main/java/com/demo/falabella/rest/demo_rest/utilss/QueueUtils.java
```
en la linea 36 modificar la ip colocando la del 
contenedor (para conocer la ip del contenedor usar docker inspect {id_contenedor}) 
```bash
cf.setStringProperty(WMQConstants.WMQ_HOST_NAME, "{ip_contenedor:ex=10.10.0.6}");
```
## Instalación de la base de datos
Se utilizó db 2 como gestor de base de datos y el comando para levantar un contenedor docker de esta BD es el siguiente

```bash
sudo docker run -itd --name db2 --privileged=true -p 50001:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=ACL.2019 -e DBNAME=testdb -v /home/nparedes/volumenes/db2:/database ibmcom/db2
```
La sentencia sql que se utilizo para generar la tabla es el siguiente:

```sql
CREATE TABLE SYSPROC.CLIENTES (
GENERO VARCHAR(30),
NOMBRE VARCHAR(150),
APELLIDO VARCHAR(150), 
CIUDAD VARCHAR(100),
PAIS VARCHAR(100),
FECHA__NACIMIENTO DATE,
CELULAR VARCHAR(50)
)
```
## Argumentos de ejecución
El aplicativo puede ingresar un cliente en una cola mq, insertarlo en una base de datos o ambas al mismo tiempo. para configurar eso hay que entregarle 2 argumentos, los cuales son:

- enviar-a-cola
- insertar-en-db2

El orden de los argumentos no afecta su funcionamiento y deben ir separados por espacios en blanco. 
